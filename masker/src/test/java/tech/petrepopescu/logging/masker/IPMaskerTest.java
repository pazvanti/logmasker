package tech.petrepopescu.logging.masker;


import tech.petrepopescu.logging.masker.abstracts.AbstractIpMaskerTest;

public class IPMaskerTest extends AbstractIpMaskerTest {
    @Override
    protected LogMasker getLogMasker(String params) {
        LogMasker ipMasker = new IPMasker();
        ipMasker.initialize(params);
        return ipMasker;
    }
}
