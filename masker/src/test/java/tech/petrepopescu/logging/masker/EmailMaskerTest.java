package tech.petrepopescu.logging.masker;

import tech.petrepopescu.logging.masker.abstracts.AbstractEmailMaskerTest;

public class EmailMaskerTest extends AbstractEmailMaskerTest {
    @Override
    protected LogMasker getLogMasker(String params) {
        LogMasker emailMasker = new EmailMasker();
        emailMasker.initialize(params);
        return emailMasker;
    }
}
