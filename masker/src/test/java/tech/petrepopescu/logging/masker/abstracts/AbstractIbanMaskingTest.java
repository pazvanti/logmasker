package tech.petrepopescu.logging.masker.abstracts;

import tech.petrepopescu.logging.MaskingConverter;
import tech.petrepopescu.logging.masker.LogMasker;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;

public abstract class AbstractIbanMaskingTest extends AbstractMaskingTest {
    protected MaskingConverter masker = initializeConverter();

    @ParameterizedTest
    @CsvSource(value = {
            "this is a iban SC52BAHL01031234567890123456USD and it should be masked|this is a iban SC5************************6USD and it should be masked",
            "this is a iban PT50002700000001234567833|this is a iban PT5******************7833", // IBAN at the end
            "KW81CBKU0000000000001234560101 this is a iban|KW8***********************0101 this is a iban", // IBAN at the beginning
            "NO8330001234567|NO8********4567", // IBAN alone
            "iban=NO8330001234567|iban=NO8********4567", // IBAN
            "<iban>NO8330001234567</iban>|<iban>NO8********4567</iban>", // IBAN alone
            // Multi-line
            "\'this is a iban NL02ABNA0123456789 and it should be masked\nAnd another iban MU43BOMM0101123456789101000MUR and it should be masked\'|\'this is a iban NL0***********6789 and it should be masked\nAnd another iban MU4***********************0MUR and it should be masked\'",
            // end of multi-line
            "\'this is a iban NL02ABNA0123456789\nAnd another iban MU43BOMM0101123456789101000MUR\'|\'this is a iban NL0***********6789\nAnd another iban MU4***********************0MUR\'",
            // same iban multiple times multi-line
            "\'this is a iban NL02ABNA0123456789\nthis is a iban NL02ABNA0123456789\nthis is a iban NL02ABNA0123456789\nthis is a iban NL02ABNA0123456789\nthis is a iban NL02ABNA0123456789\nthis is a iban NL02ABNA0123456789\'|\'this is a iban NL0***********6789\nthis is a iban NL0***********6789\nthis is a iban NL0***********6789\nthis is a iban NL0***********6789\nthis is a iban NL0***********6789\nthis is a iban NL0***********6789\'",

    }, delimiter = '|')
    public void standardMaskingTests(String unmasked, String expected) {
        final StringBuilder buffer = new StringBuilder(unmasked);
        masker.mask(buffer);
        Assertions.assertEquals(expected, buffer.toString());
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "This is not an IBAN SC52BAHL01031234567890123456USDD so no masking",
            "This is not an IBAN SCDDBAHL01031234567890123456USD so no masking",
            "This is not an IBAN SC52BAHL01031234567890123456 so no masking",
            "This is not an IBAN LL52LLLL000000 so no masking",
            "SC52BAHL01031234567890123456USDDD",
            "SCDDBAHL01031234567890123456USD",
            "SC52BAHL01031234567890123456",
            "LL52LLLL000000",
    })
    public void looksLikeIbanButItIsNot(String unmasked) {
        final StringBuilder builder = new StringBuilder(unmasked);
        masker.mask(builder);
        Assertions.assertEquals(unmasked, builder.toString());
    }

    @Test
    public void onlyCertainCountriesAreMasked() {
        final String unmasked = "this is a iban FO9264600123456789 and it should be masked\nthis is a iban EG800002000156789012345180002 and it should be masked";
        final String expected = "this is a iban FO9264600123456789 and it should be masked\nthis is a iban EG8**********************0002 and it should be masked";
        final StringBuilder buffer = new StringBuilder(unmasked);

        LogMasker masker = getLogMasker("EG");
        MaskingConverter converter = new MaskingConverter();
        converter.setMaskers(Arrays.asList(masker));

        converter.mask(buffer);
        Assertions.assertEquals(expected, buffer.toString());
    }

    @Test
    public void testJson() {
        final String unmasked = "{\n" +
                "   \"password\":\"1234\",\n" +
                "   \"iban\":\"NL02ABNA0123456789\"\n" +
                "}";
        final String expected = "{\n" +
                "   \"password\":\"1234\",\n" +
                "   \"iban\":\"NL0***********6789\"\n" +
                "}";

        final StringBuilder buffer = new StringBuilder(unmasked);

        masker.mask(buffer);
        Assertions.assertEquals(expected, buffer.toString());
    }
}
