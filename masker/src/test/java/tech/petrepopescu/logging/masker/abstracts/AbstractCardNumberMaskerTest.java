package tech.petrepopescu.logging.masker.abstracts;

import tech.petrepopescu.logging.MaskingConverter;
import tech.petrepopescu.logging.masker.LogMasker;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;

public abstract class AbstractCardNumberMaskerTest extends AbstractMaskingTest {
    protected MaskingConverter maskingConverter = initializeConverter();

    @ParameterizedTest
    @CsvSource(value = {
            "this is a card 4916246076443617 and it should be masked|this is a card 4*********443617 and it should be masked", // standard masking
            "this is a card 4916246076443617|this is a card 4*********443617", // Card at the end
            "4916246076443617|4*********443617", // card alone
            "cardNumber=4111111111111111|cardNumber=4*********111111",
            // multi-line
            "\'this is a card 4916246076443617 and it should be masked\nthis is a card 3337771334518002123 and it should be masked\'|\'this is a card 4*********443617 and it should be masked\nthis is a card 3************002123 and it should be masked\'",
            // multi-line end of line
            "\'this is a card 4916246076443617\nthis is a card 4916246076443617\'|\'this is a card 4*********443617\nthis is a card 4*********443617\'",
            // JSON
            "\'{\"password\":\"1234\",\"card\":\"4916246076443617\"}\'|\'{\"password\":\"1234\",\"card\":\"4*********443617\"}\'",
            "<cardNumber>4916246076443617</cardNumber>|<cardNumber>4*********443617</cardNumber>"
    }, delimiter = '|')
    void maskingTests(String unmasked, String expected) {
        final StringBuilder buffer = new StringBuilder(unmasked);
        maskingConverter.mask(buffer);
        Assertions.assertEquals(expected, buffer.toString());
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "this is not a card 9916246076443617 and it shouldn't be masked",
            "this is not a card 4916246 and it shouldn't be masked",
            "4916246",
    })
    public void nonCardIsNotMasked(String unmasked) {
        final StringBuilder buffer = new StringBuilder(unmasked);
        maskingConverter.mask(buffer);
        Assertions.assertEquals(unmasked, buffer.toString());
    }

    @Test
    public void validateMaskingWhenNonDefaultStartAndEnd() {
        final String unmasked = "this is a card 4916246076443617 and it should be masked";
        final String expected = "this is a card 49**********3617 and it should be masked";
        final StringBuilder buffer = new StringBuilder(unmasked);

        LogMasker masker = getLogMasker("2|4");
        MaskingConverter converter = new MaskingConverter();
        converter.setMaskers(Arrays.asList(masker));

        converter.mask(buffer);
        Assertions.assertEquals(expected, buffer.toString());
    }

    @Test
    public void testJsonFormatted() {
        final String unmasked = "{\n" +
                "   \"password\":\"1234\",\n" +
                "   \"card\":\"4916246076443617\"\n" +
                "}";
        final String expected = "{\n" +
                "   \"password\":\"1234\",\n" +
                "   \"card\":\"4*********443617\"\n" +
                "}";

        final StringBuilder buffer = new StringBuilder(unmasked);

        maskingConverter.mask(buffer);
        Assertions.assertEquals(expected, buffer.toString());
    }

    @ParameterizedTest
    @CsvSource(value = {
            "3|4|5;Invalid parameters supplied for CardNumber masker",
            "3;Invalid parameters supplied for CardNumber masker",
            "0|6;The number of unmasked digits at the start of the pan can't be more than 6 or less than 1",
            "-1|6;The number of unmasked digits at the start of the pan can't be more than 6 or less than 1",
            "9|6;The number of unmasked digits at the start of the pan can't be more than 6 or less than 1",
            "1|0;The number of unmasked digits at the end of the pan can't be more than 8 or less than 1",
            "1|-1;The number of unmasked digits at the end of the pan can't be more than 8 or less than 1",
            "1|9;The number of unmasked digits at the end of the pan can't be more than 8 or less than 1"
    }, delimiter = ';')
    public void validateInitExceptions(String params, String errorContains) {
        LogMasker newMasker = getLogMasker();

        boolean exceptionThrown = false;
        try {
            newMasker.initialize(params);
        } catch (ExceptionInInitializerError ex) {
            Assertions.assertTrue(ex.getMessage().contains(errorContains));
            exceptionThrown = true;
        }

        Assertions.assertTrue(exceptionThrown);
    }
}
