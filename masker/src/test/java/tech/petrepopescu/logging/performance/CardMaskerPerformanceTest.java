package tech.petrepopescu.logging.performance;

import tech.petrepopescu.logging.masker.CardNumberMasker;
import tech.petrepopescu.logging.masker.LogMasker;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class CardMaskerPerformanceTest extends AbstractPerformanceTest {
    @Test
    void cardPerformanceTest() {
        List<LogMasker> maskers = Arrays.asList(new CardNumberMasker());
        double min = maskerPerformanceComparisonTests(maskers);

        Assertions.assertTrue(min < 1000d);
    }
}
