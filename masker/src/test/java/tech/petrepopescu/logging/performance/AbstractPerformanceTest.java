package tech.petrepopescu.logging.performance;

import tech.petrepopescu.logging.MaskingConverter;
import tech.petrepopescu.logging.masker.LogMasker;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Performance test suite that can be used to check execution times and compare two or more maskers.
 * Useful if you are writing a new version of a masker and want to check how it performs compared to the existing one
 */
public abstract class AbstractPerformanceTest {
    protected static final List<String> LOG_TEST_LINES = Arrays.asList("This will mask the email test.email@domain.com",
            "This will mask the email testemail@domain.com",
            "This is a password: testpass",
            "Another password:testpass2. That will be masked",
            "Nothing will be masked here",
            "this is another IP 84.232.150.27 and nothing more",
            "this is another IP 1.1.1.1 and nothing more",
            "this is a card 4916246076443617 and it should be masked",
            "this is a card 377235414017308 and it should be masked",
            "this is a iban IE64IRCE92050112345678 and it should be masked",
            "this is a iban GT20AGRO00000000001234567890 and it should be masked");

    private static final int TOTAL_RUNS = 10;

    private static final int WARMUP_RUNS = 10;
    private static final int NUMBER_OF_LINES = 100_000;
    private static final boolean LOG_INDIVIDUAL_RUNS = false;

    public double maskerPerformanceComparisonTests(List<LogMasker> maskers) {
        Map<String, Double> timings = new HashMap<>();

        for (LogMasker masker:maskers) {
            String testName = masker.getClass().getName();
            double result = performanceTest(LOG_TEST_LINES, masker, testName);
            timings.put(testName, result);
        }

        double min = Double.MAX_VALUE;
        String fastestTest = "";
        for(Map.Entry<String, Double> result:timings.entrySet()) {
            if (min > result.getValue()) {
                min = result.getValue();
                fastestTest = result.getKey();
            }
        }

        System.out.println("Fastest test was " + fastestTest + " with a time of " + min);

        return min;
    }

    public double converterPerformanceTest(MaskingConverter maskingConverter) {
        Consumer<String> buildAndMaskEvent = (String line) -> {
            StringBuilder stringBuffer = new StringBuilder(line);
            maskingConverter.mask(stringBuffer);
        };

        return performanceTest(LOG_TEST_LINES, buildAndMaskEvent, maskingConverter.getClass().getName());
    }

    protected double performanceTest(List<String> lines, LogMasker masker, String testName) {
        MaskingConverter maskingConverter = new MaskingConverter();
        maskingConverter.setMaskers(Arrays.asList(masker));

        Consumer<String> buildAndMaskEvent = (String line) -> {
            StringBuilder stringBuffer = new StringBuilder(line);
            maskingConverter.mask(stringBuffer);
        };

        return performanceTest(lines, buildAndMaskEvent, testName);
    }

    protected double performanceTest(List<String> lines, Consumer<String> maskerCallerFunction, String testName) {
        System.out.println("Starting performance test for " + testName + "\n\tTotal runs: " + TOTAL_RUNS + "\n\tNumber of lines per run: " + NUMBER_OF_LINES);
        final List<Long> executionTimes = new ArrayList<>();
        for (int run = 0; run < WARMUP_RUNS; run++) {
            List<String> toMask = new ArrayList<>();
            Random random = new Random();
            for (int count = 0; count < NUMBER_OF_LINES; count++) {
                toMask.add(lines.get(random.nextInt(lines.size())));
            }

            for (String line:toMask) {
                maskerCallerFunction.accept(line);
            }
        }
        for (int run = 0; run < TOTAL_RUNS; run++) {
            List<String> toMask = new ArrayList<>();
            Random random = new Random();
            for (int count = 0; count < NUMBER_OF_LINES; count++) {
                toMask.add(lines.get(random.nextInt(lines.size())));
            }

            long start = System.nanoTime();
            for (String line:toMask) {
                maskerCallerFunction.accept(line);
            }

            long end = System.nanoTime();
            long time = (end-start)/1_000_000;
            executionTimes.add(time);
            if (LOG_INDIVIDUAL_RUNS) {
                System.out.println("Duration for run " + run + " was: " + time + "ms");
            }
        }

        long totalTime = executionTimes.stream().collect(Collectors.summingLong(Long::longValue));
        double averageTime = ((double)totalTime)/((double) TOTAL_RUNS);
        System.out.println("Average duration was: " + averageTime + "ms\n");

        return averageTime;
    }
}
