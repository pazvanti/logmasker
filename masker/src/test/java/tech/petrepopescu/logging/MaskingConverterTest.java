package tech.petrepopescu.logging;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class MaskingConverterTest {
    @ParameterizedTest
    @CsvSource(value = {
            ",5",
            "email;card,2",
            "email,1",
            "all,5",
            "card:1|4,1",
    })
    void initializationTests(String options, int noMaskers) throws NoSuchFieldException, IllegalAccessException {
        List<String> optionsParsed;
        if (options != null) {
            optionsParsed = Arrays.asList(options.split(";"));
        }  else {
            optionsParsed = Collections.emptyList();
        }

        MaskingConverter maskingConverter = new MaskingConverter();
        maskingConverter.init(optionsParsed);

        validateNumberOfMaskers(maskingConverter, noMaskers);
    }

    @Test
    void nullOptionsTest() throws NoSuchFieldException, IllegalAccessException {
        MaskingConverter maskingConverter = new MaskingConverter();
        maskingConverter.init(null);

        validateNumberOfMaskers(maskingConverter, 5);
    }

    @Test
    void testInvalidOption() {
        boolean exceptionThrown = false;
        MaskingConverter maskingConverter = new MaskingConverter();

        try {
            maskingConverter.init(Arrays.asList("email", "invalid"));
        } catch (ExceptionInInitializerError ex) {
            Assertions.assertEquals("Invalid option provided: invalid", ex.getMessage());
            exceptionThrown = true;
        }

        Assertions.assertTrue(exceptionThrown);
    }

    @Test
    void testMaskerIsCalled() {
        MaskingConverter maskingConverter = new MaskingConverter();
        maskingConverter.init(Arrays.asList("email"));

        StringBuilder builder = new StringBuilder("This will mask the email test.email@domain.com and it should be masked");
        maskingConverter.mask(builder);

        Assertions.assertEquals("This will mask the email t********l@d****n.com and it should be masked", builder.toString());
    }

    @ParameterizedTest
    @CsvSource(value = {
            "custom|com.test.me,5",
            "custom|com.test.me:4|2,5",
            "email;custom|com.test.me,1",
            "email;custom|com.test.me:4|2,1",
    })
    void testNonexistentCustomMasker(String options, String noMaskers) throws NoSuchFieldException, IllegalAccessException {
        List<String> optionsParsed = Arrays.asList(options.split(";"));
        int noMaskersParsed = Integer.parseInt(noMaskers);

        MaskingConverter maskingConverter = new MaskingConverter();
        maskingConverter.init(optionsParsed);

        validateNumberOfMaskers(maskingConverter, noMaskersParsed);
    }

    private void validateNumberOfMaskers(MaskingConverter maskingConverter, int noMaskers) throws NoSuchFieldException, IllegalAccessException {
        Field field = MaskingConverter.class.getDeclaredField("maskers");
        field.setAccessible(true);

        int actualMaskersNumber = ((List)field.get(maskingConverter)).size();

        Assertions.assertEquals(noMaskers, actualMaskersNumber);
    }

    private void validateExclusiveIsSet(MaskingConverter maskingConverter) throws NoSuchFieldException, IllegalAccessException {
        Field field = MaskingConverter.class.getDeclaredField("exclusive");
        field.setAccessible(true);

        boolean exclusiveValue = (Boolean)field.get(maskingConverter);

        Assertions.assertTrue(exclusiveValue);
    }
}
