package tech.petrepopescu.logging.logback.maskers;

import tech.petrepopescu.logging.masker.CardNumberMasker;

public class LogbackCardNumberMasker extends CardNumberMasker implements LogbackLogMasker {
    public void setStartKeep(int startKeep) {
        if (startKeep > 0 && startKeep < 7) {
            this.startKeep = startKeep;
        }
    }

    public void setEndKeep(int endKeep) {
        if (endKeep > 0 && endKeep < 9) {
            this.endKeep = endKeep;
        }
    }

    public void setLuhnCheck(boolean luhnCheck) {
        this.luhnCheck = luhnCheck;
    }
}
