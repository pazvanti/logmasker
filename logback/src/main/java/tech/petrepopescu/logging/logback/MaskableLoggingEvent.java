package tech.petrepopescu.logging.logback;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.IThrowableProxy;
import ch.qos.logback.classic.spi.LoggerContextVO;
import org.slf4j.Marker;
import org.slf4j.helpers.MessageFormatter;

import java.util.Map;

public class MaskableLoggingEvent implements ILoggingEvent {
    private String message;

    private String formattedMessage;

    private ILoggingEvent original;

    public MaskableLoggingEvent(ILoggingEvent original) {
        this.original = original;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String getThreadName() {
        return original.getThreadName();
    }

    @Override
    public Level getLevel() {
        return original.getLevel();
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public Object[] getArgumentArray() {
        return new Object[0];
    }

    public String getFormattedMessage() {
        if (formattedMessage != null) {
            return formattedMessage;
        }
        if (original.getArgumentArray() != null) {
            formattedMessage = MessageFormatter.arrayFormat(message, original.getArgumentArray()).getMessage();
        } else {
            formattedMessage = message;
        }

        return formattedMessage;
    }

    @Override
    public String getLoggerName() {
        return original.getLoggerName();
    }

    @Override
    public LoggerContextVO getLoggerContextVO() {
        return original.getLoggerContextVO();
    }

    @Override
    public IThrowableProxy getThrowableProxy() {
        return original.getThrowableProxy();
    }

    @Override
    public StackTraceElement[] getCallerData() {
        return original.getCallerData();
    }

    @Override
    public boolean hasCallerData() {
        return original.hasCallerData();
    }

    @Override
    public Marker getMarker() {
        return original.getMarker();
    }

    @Override
    public Map<String, String> getMDCPropertyMap() {
        return original.getMDCPropertyMap();
    }

    @Override
    public Map<String, String> getMdc() {
        return original.getMDCPropertyMap();
    }

    @Override
    public long getTimeStamp() {
        return original.getTimeStamp();
    }

    @Override
    public void prepareForDeferredProcessing() {
        original.prepareForDeferredProcessing();
    }
}
