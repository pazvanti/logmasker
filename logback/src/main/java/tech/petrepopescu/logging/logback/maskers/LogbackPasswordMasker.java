package tech.petrepopescu.logging.logback.maskers;

import tech.petrepopescu.logging.masker.PasswordMasker;

import java.util.ArrayList;
import java.util.List;

public class LogbackPasswordMasker extends PasswordMasker implements LogbackLogMasker {
    private List<String> logbackKeywords = new ArrayList<>();

    public void addKeyword(String keyword) {
        if (keyword != null) {
            this.logbackKeywords.add(keyword);
            this.keywords.clear();
            this.keywords.addAll(this.logbackKeywords);
        }
    }
}
