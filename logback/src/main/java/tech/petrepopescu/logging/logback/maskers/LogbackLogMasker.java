package tech.petrepopescu.logging.logback.maskers;

import tech.petrepopescu.logging.masker.LogMasker;

public interface LogbackLogMasker extends LogMasker {
    default LogMasker toLogMasker() {
        return this;
    }
}
